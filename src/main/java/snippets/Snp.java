package snippets;

public class Snp {

    private long position;
    private char referenceNucleotide;
    private char alternativeNucleotide;

    public Snp(long position, char reference, char alternative) {
        if (position < 1) {
            throw new IllegalArgumentException("position must be positive");
        }
        this.position = position;
        this.referenceNucleotide = reference;
        this.alternativeNucleotide = alternative;
    }

    public long getPosition() {
        return position;
    }

    public char getReferenceNucleotide() {
        return referenceNucleotide;
    }

    public char getAlternativeNucleotide() {
        return alternativeNucleotide;
    }

    public boolean isTransition() {
        return ((referenceNucleotide == 'A' && alternativeNucleotide == 'G')
                || (referenceNucleotide == 'C' && alternativeNucleotide == 'T'));
    }
}
