package nl.bioinf.ojweeda.ninjas;

public class Game {
    public static void main(String[] args) {
        Ninja ninja = new Ninja();

        ninja.name = "Rogue Bastard";

        System.out.println("Ninja name      = " + ninja.name);
        System.out.println("Ninja energy    = " + ninja.energyLevel);
        System.out.println("Ninja position  = ["
                + ninja.topCoordinate
                + ":"
                + ninja.leftCoordinate
                + "]");

        GameCharacter opponent = new GameCharacter();
        opponent.name = "Delirious Troll";


        ninja.move(2, 9);
        ninja.move(-6.4, 3.5);
        System.out.println("Ninja position  = ["
                + ninja.topCoordinate
                + ":"
                + ninja.leftCoordinate
                + "]");

        ninja.attack(5, opponent);

        System.out.println("Ninja energy    = " + ninja.energyLevel);
        System.out.println("Opponent name   = " + opponent.name);
        System.out.println("Opponent energy = " + opponent.energyLevel);
    }

}
