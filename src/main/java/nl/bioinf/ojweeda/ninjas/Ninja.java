package nl.bioinf.ojweeda.ninjas;

public class Ninja {


    String name;
    int energyLevel = 100;
    double topCoordinate;
    double leftCoordinate;

    void move(double top, double left) {
        this.topCoordinate += top;
        this.leftCoordinate += left;
    }

    void attack(int power, GameCharacter opponent) {
        this.energyLevel -= power;
        opponent.drainEnergy(power * 2);
    }

}
